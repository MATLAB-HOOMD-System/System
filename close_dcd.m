
function rc = close_dcd(h)
% This software includes code developed by the Theoretical and Computational
% Biophysics Group in the Beckman Institute for Advanced Science and Technology 
% at the University of Illinois at Urbana-Champaign.

% rc = close_dcd(handle)

rc = fclose(h.fid);



