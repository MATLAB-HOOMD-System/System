function system=remove_beads(system,beadnums)
%removes beads of index beadnum. Requires system as structure variable.


system.attype(beadnums,:)=[];
system.mass(beadnums,:)=[];
system.pos(beadnums,:,:)=[];


if isfield(system,'bonds');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.bonds(find(sum(system.bonds==b-1,2),1e20),:)=[];
        system.bonds(system.bonds>b-1)=system.bonds(system.bonds>b-1)-1;
        beadnums(beadnums>b)=beadnums(beadnums>b)-1;
    end
end

if isfield(system,'angles');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.angles(find(sum(system.angles==b-1,2),1e20),:)=[];
        system.angles(system.angles>b-1)=system.angles(system.angles>b-1)-1;
    end
end

if isfield(system,'diameter');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.diameter(b,:)=[];
        beadnums(beadnums>b)=beadnums(beadnums>b)-1;
    end
end

if isfield(system,'charge');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.charge(b,:)=[];
    end
end

if isfield(system,'body');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.body(b,:)=[];
        beadnums(beadnums>b)=beadnums(beadnums>b)-1;
    end
end

if isfield(system,'vel');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.vel(b,:,:)=[];
        beadnums(beadnums>b)=beadnums(beadnums>b)-1;
    end
end

if isfield(system,'img');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.img(b,:,:)=[];
        beadnums(beadnums>b)=beadnums(beadnums>b)-1;
    end
end

if isfield(system,'moment_inertia');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.moment_inertia(b,:,:)=[];
        beadnums(beadnums>b)=beadnums(beadnums>b)-1;
    end
end

if isfield(system,'orientation');
    for i=1:length(beadnums);
        b=beadnums(i);
        system.orientation(b,:,:)=[];
        beadnums(beadnums>b)=beadnums(beadnums>b)-1;
    end
end



if isfield(system,'natoms')
    system.natoms=size(system.attype,1);
end