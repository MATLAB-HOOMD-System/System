function chainstats=Chain_Statistics_single(Input_File,system,packflag)
% Finds chain statistics for every chain in a system.
if Input_File~=0
    load(Input_File);
else
    systemexpand_script;
end
if exist('packflag','var')==0
    packflag=1;
end

pos=pos(:,:,1);
chainstats=struct('nchains',{},'nbeads_per_chain',{},'chain',{},'chains2',{},...
    'Mn',{},'Mw',{},'PDI',{},'Rg',{},'Rg_Tensors',{},'Rg_eigenvalues',{},'End_to_End_Distance',{},'bond_lengths',{},'angles',{},'orientedRgX_eigenvalues',{},'orientedRgY_eigenvalues',{},'orientedRgZ_eigenvalues',{});

nsteps=size(pos,3);

% Determine which bead is in which chain, put in expanded form
[pos bonds nbonds_per_atom nchains nchains2 chain nbeads_per_chain chains2]=chain_stats_packing(pos,bonds,dim);
if packflag==0
    pos=system.pos;
elseif packflag~=1
    error('packflag set to something other than 0 or 1')
end
chainstats(1).nchains=nchains;
chainstats(1).chain=chain;
chainstats(1).chains2=chains2;
chainstats(1).nbeads_per_chain=nbeads_per_chain;

% Calculate Mn, Mw, and PDI
chainstats(1).Mn=sum(nbeads_per_chain.*repmat(1/nchains,nchains,1))/sum(repmat(1/nchains,nchains,1));
chainstats(1).Mw=sum(nbeads_per_chain.^2.*repmat(1/nchains,nchains,1))/sum(nbeads_per_chain.*repmat(1/nchains,nchains,1));
chainstats(1).PDI=chainstats(1).Mw/chainstats(1).Mn;

% Calculate Radius of Gyration- Polymer Definintion
Rg_Tensors=zeros(3,3,nchains2,nsteps);
Rg_eigenvalues=zeros(3,3,nchains2,nsteps);
cpos=zeros(nchains2,3,nsteps);

for i=1:nchains2
%     Xmaxmin(i)=0;
%     Ymaxmin(i)=0;
    pos2=pos(chains2(1:nbeads_per_chain(i),i),:,:);
    cpos(i,:,:)=mean(pos2);
    pos2=pos2-repmat(cpos(i,:,:),[nbeads_per_chain(i),1,1]);
    for k=1:nsteps
        Xmaxmin(i,k)=max(pos2(:,1))-min(pos2(:,1));%+Xmaxmin(i);
        Ymaxmin(i,k)=max(pos2(:,2))-min(pos2(:,2));
        Zmaxmin(i,k)=max(pos2(:,3))-min(pos2(:,3));%+Ymaxmin(i);
        M=pos2(:,:,k)'*pos2(:,:,k);
        [Rg_Tensors(:,:,i,k) Rg_eigenvalues(:,:,i,k)]=eig(M);
    end
end
% Xmaxmin=Xmaxmin/nsteps;
Rg_eigenvalues=[reshape(Rg_eigenvalues(1,1,:,:),nchains2,1,nsteps) reshape(Rg_eigenvalues(2,2,:,:),nchains2,1,nsteps) reshape(Rg_eigenvalues(3,3,:,:),nchains2,1,nsteps)];
Rg_eigenvalues=Rg_eigenvalues./repmat(nbeads_per_chain(1:nchains2),[1,3,nsteps]);
% si=cat(2,Rg_eigenvalues(:,3,:)./Rg_eigenvalues(:,1,:),Rg_eigenvalues(:,2,:)./Rg_eigenvalues(:,1,:));
Rg=sqrt(sum(Rg_eigenvalues,2));
chainstats(1).Rg_Tensors=Rg_Tensors;
chainstats(1).Rg_eigenvalues=Rg_eigenvalues;
chainstats(1).Rg=Rg;
if exist('Xmaxmin','var')==1
    chainstats.orientedRgX_eigenvalues=Xmaxmin;
    chainstats.orientedRgY_eigenvalues=Ymaxmin;
    chainstats.orientedRgZ_eigenvalues=Zmaxmin;
end
% Bond Lengths
if ~isempty(bonds)
    bond_lengths=sqrt(sum((pos(bonds(:,1),:,:)-pos(bonds(:,2),:,:)).^2,2));
    chainstats(1).bond_lengths=bond_lengths;
end

% Angle Stuff
if size(angles,1)==0
    angles=[];
end
if ~isempty(angles)% ~= []
	angles=angles+1;
	vec1=pos(angles(:,1),:,:)-pos(angles(:,2),:,:);
	vec2=pos(angles(:,3),:,:)-pos(angles(:,2),:,:);
	vec1_mag=sqrt(sum(vec1.^2,2));
	vec2_mag=sqrt(sum(vec2.^2,2));
	angles=acos(dot(vec1,vec2,2)./vec1_mag./vec2_mag);
	chainstats(1).angles=angles;
end

End_to_End_Distance=zeros(nchains2,1,nsteps);
% End to End Distance
for i=1:nchains2
    x=chains2(1:nbeads_per_chain(i),i);
    y=find(nbonds_per_atom(x,1)==1);
    End_to_End_Distance(i,:,:)=sqrt(sum((pos(x(y(1)),:,:)-pos(x(y(2)),:,:)).^2,2));
end
chainstats(1).End_to_End_Distance=End_to_End_Distance;

end
