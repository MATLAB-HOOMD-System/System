function [attype,bonds,dim,mass,pos,angles, chainstats,body,velocity,orientation,moment_inertia,angmom,diameter]=systemexpand(system)
attype=system.attype;
bonds=system.bonds;
dim=system.dim;
mass=system.mass;
pos=system.pos;

if isfield(system,'diameter')
    diameter=system.diameter;;
else
    diameter=[];
end

if isfield(system,'angles')
    angles=system.angles;;
else
    angles=[];
end

if isfield(system,'chainstats')
    chainstats=system.chainstats;
else
    chainstats=[];
end
if isfield(system,'body')
    body=system.body;
else
    body=[];
end

if isfield(system,'velocity')
    velocity=system.velocity;
else
    velocity=[];
end

if isfield(system,'orientation')
    orientation=system.orientation;
else
    orientation=[];
end

if isfield(system,'moment_inertia')
    moment_inertia=system.moment_inertia;
else
    moment_inertia=[];
end

if isfield(system,'angmom')
    angmom=system.angmom;
else
    angmom=[];
end