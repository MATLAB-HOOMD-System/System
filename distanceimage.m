function [dis] = distanceimage(pos,dim,square)
%minimumdistanceimage finds the distance between all particles in pos using the minimum image
%convention. Box is centered at 0 0 0. If square is 1 it outputs an nxn
%array. Otherwise it outputs a list of all distances.  

if ~exist('square','var')
    square=1;
end
 
n=size(pos,1);

if square==1
    for i=1:n
        for j=1:n
            if i==j
                dis(i,j)=NaN;
            else
                disxyz=pos(i,:)-pos(j,:);
                for p=1:3
                    if abs(disxyz(p))>dim(p)/2;
                        disxyz(p)=dim(p)-abs(disxyz(p));
                    end
                end
                dis(i,j)=sqrt(sum((disxyz).^2));
            end
        end
    end
else
    counter=0;
    for i=1:n
%         i
        for j=i+1:n
            counter=counter+1;
                disxyz=pos(i,:)-pos(j,:);
                for p=1:3
                    if abs(disxyz(p))>dim(p)/2;
                        disxyz(p)=dim(p)-abs(disxyz(p));
                    end
                end
                dis(counter)=sqrt(sum((disxyz).^2));
        end
    end
end
end

