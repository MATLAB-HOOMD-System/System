function dom=domain_calculator(r,gofr,density,dim)
x=gofr==0;
I=find(x==0,1,'last');
r=r(1:I);
gofr=gofr(1:I);
d=1:0.001:min(dim)/2;
q=2*pi./d;
nq=length(q);
S=zeros(size(q));
for i=1:nq
    S(i)=1+4*pi*density*trapz(r,r.^2.*sin(q(i)*r)./(q(i)*r).*(gofr-1).*sin(pi*r/r(end))./(pi*r/r(end)));
end
[~,I]=max(S);
dom=2*pi/q(I);
pererror=19.054*(dom/r(end))^2.4676;
dom=dom/(1+pererror/100);

end