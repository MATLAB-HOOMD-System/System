function [system] = Keep_Steps(system,Steps,varargin)
%Changes steps kept in system. Steps are defined relative to initial step and to step size. To keep/remove absolute steps, set parameters 'Absolute_steps' to 1. To remove steps instead, set parameter
%'Remove_steps' to 1 (Keep_Steps(gsdname,Steps,'Remove_steps',1). If Steps
%are specified outside steps that exist, an error will be thrown. INDEXING STARTS AT 0. To set indexing to start at 1, set 'FirstStep' to 1.  


%Setupinput parser
p=inputParser;
defaultRemove_steps=0;
defaultAbsolute_steps=0;
defaultFirstStep=0;
addParameter(p,'Remove_steps',defaultRemove_steps,@isnumeric)
addParameter(p,'Absolute_steps',defaultAbsolute_steps,@isnumeric)
addParameter(p,'FirstStep',defaultFirstStep,@isnumeric)
parse(p,varargin{:});
fields=fieldnames(p.Results);
for i=1:numel(fields)
    eval([fields{i} '=' 'p.Results.' fields{i} ';']);
end

if Absolute_steps==1 && FirstStep==1
    error('Absolute Steps and First Step = 1 cannot both be specified')
end

if min(Steps(2:end)-Steps(1:end-1)<1)
    error('Steps are not monotonic')
end

if FirstStep>1 || FirstStep<0
    error('Invalid FirstStep. Should be 1 or 0')
end

if Absolute_steps==1
    Steps=Steps-initialstep;
    for i=2:length(Steps)
        Steps(i)=(Steps(i+1)-Steps(i))*system.stepsize+Steps(i);
    end
end

if Remove_steps==1
    Steps=setdiff(0:size(system.pos,3),Steps);
end


if FirstStep==0
    Steps=Steps+1;
end

if size(system.pos,3)>max(Steps) || min(Steps<1)
    error('Steps out of Bounds')
end

if Steps(end)~=size(system.pos,3)
   system.timestep=(size(system.pos,3) - Steps(end))*system.stepsize;
end
    

system.pos=system.pos(:,:,Steps);
if isfield(system,'vel')
    system.vel=system.vel(:,:,Steps);
end
if isfield(system,'img')
    system.img=system.img(:,:,Steps);
end
if isfield(system,'moment_inertia')
    system.moment_inertia=system.moment_inertia(:,:,Steps);
end
if isfield(system,'orientation')
    system.orientation=system.orientation(:,:,Steps);
end

if length(unique(Steps(2:end)-Steps(1:end-1)))~=1
    system.stepsize=NaN;
    warning('Step Size not uniform')
else
    system.stepsize=unique(Steps(2:end)-Steps(1:end-1))*system.stepsize;
end

if Steps(1)~=1
    system.initialstep=system.initialstep+(Steps(1)-1)*system.stepsize;
end


    