function [system] = Combine_Updated(system1,system2)
%Combines Muliple systems Only some subsets supported


system.natoms=system1.natoms+system2.natoms;
system.attype=[system1.attype system2.attype];
system.mass=[system1.mass system2.mass];
system.bonds=[system1.bonds; system2.bonds+max(system1.natoms)];
if isfield(system1,'angles')
    system.angles=[system1.angles; system2.angles+max(system1.natoms)];
end
system.dim=[max(system1.dim(1),system2.dim(1)) max(system1.dim(2),system2.dim(2)) max(system1.dim(3),system2.dim(3))];
system.pos=[system1.pos;system2.pos];


end

