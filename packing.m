function pos=packing(pos,bonds,dim,pack)
% Packs beads into a periodic box, or unwraps them so that chains are not
% spanning the periodic boundary. In the unwrapping case, the center of
% mass for the chain is placed within the periodic box.
% 
% pos-> position matrix, of the size (number atoms,3,number time steps)
% bonds-> bond matrix, of the size (number bonds,2)
% dim-> vector telling the size of each dimension-> [xdim, ydim, zdim]
% pack->vector telling whether the positions should be close packed or
%   normal packed for each dimension. 0 means normal packed (extending
%   outside the periodic box) and 1 means close packed (wrapped around the
%   periodic boundary).

nat=size(pos,1);
nsteps=size(pos,3);
nbonds=size(bonds,1);

% Put all into Normal Packed
cut=dim/2;
normal=(1:3).*(1-pack);
normal(normal==0)=[];


if ~isempty(normal)
    bonds=bonds+1;
    chain=zeros(nat,1);
    nchains=0;
    nbonds_per_atom=zeros(nat,1);
    for i=1:nbonds
        x=bonds(i,1);
        y=bonds(i,2);
        nbonds_per_atom(x)=nbonds_per_atom(x)+1;
        nbonds_per_atom(y)=nbonds_per_atom(y)+1;
        if chain(x)==0 && chain(y)==0
            nchains=nchains+1;
            chain(x)=nchains;
            chain(y)=nchains;
        elseif chain(x)~=0 && chain(y)==0
            chain(y)=chain(x);
        elseif chain(x)==0 && chain(y)~=0
            chain(x)=chain(y);
        elseif chain(x)~=chain(y)
            z=chain(y);
            chain(chain==chain(y))=chain(x);
            chain(chain>z)=chain(chain>z)-1;
            nchains=nchains-1;
        end
    end
    x=(nbonds_per_atom==0);
    chain(x)=nchains+1:nchains+sum(x);
    nchains2=nchains;
    nchains=max(chain);
    
    [chain2 I]=sort(chain);
    pos=pos(I,:,:);
    nbeads_per_chain=zeros(nchains,1);
    j=0;
    n=1;
    for i=1:nat
        if chain2(i,1)==n
            j=j+1;
        elseif chain2(i,1)==n+1
            nbeads_per_chain(n,1)=j;
            j=1;
            n=n+1;
        end
    end
    nbeads_per_chain(n,1)=j;
    
    [~, I2]=sort(I);
    pos=pos(I2,:,:);
    % Divide into chains
    chains2=zeros(max(nbeads_per_chain),nchains);
    n=1;
    for i=1:nchains
        n2=n+nbeads_per_chain(i)-1;
        chains2(1:nbeads_per_chain(i),i)=I(n:n2,:);
        n=n2+1;
    end
    
    dimtime=repmat(dim,[1,1,nsteps]);
    for i=1:nchains
        nbeads=nbeads_per_chain(i);
        pos2=pos(chains2(1:nbeads,i),:,:);
        for j=2:nbeads
            for m=normal
                for k=1:nsteps
                    while pos2(j-1,m,k)-pos2(j,m,k)>cut(m)
                        pos2(j,m,k)=pos2(j,m,k)+dim(m);
                    end
                    while pos2(j-1,m,k)-pos2(j,m,k)<-cut(m)
                        pos2(j,m,k)=pos2(j,m,k)-dim(m);
                    end
                end
            end
        end
        cpos=mean(pos2,1);        
        pos2=pos2-repmat(floor(cpos./dimtime).*dimtime,[nbeads,1,1]);
        pos(chains2(1:nbeads,i),:,:)=pos2;
    end
end


% Put Desired Dimensions into Packed Orientations
packed=(1:3).*pack;
packed(packed==0)=[];
for j=packed
    pos(:,j,:)=(pos(:,j,:)+dim(j)/2)/dim(j);
    pos(:,j,:)=pos(:,j,:)-floor(pos(:,j,:));
    pos(:,j,:)=pos(:,j,:)*dim(j)-dim(j)/2;
end

end