path='/media/andrew/External_Data/Google Drive LATECH/simulations/Transfer_to_Shared/Nanoparticles/PCND_repulsive_3_8_18/'
path='/mnt/Shared_Data/PCND/parametertest_1/'

path='/mnt/Shared_Data/Nanoparticles/PCND_repulsive_paramscan_3_19_18/'

singlefile=1;
File='MixedN800.xml'

Files=dir(path);


if singlefile~=1
    for i=1:length(Files)
        if length(Files(i).name)>10
            if Files(i).name(end-9:end)=='_start.xml'
                savename=[path Files(i).name(1:end-10) '_vis.xml'];
                system=Extract_XML([path Files(i).name])
                for j=1:length(system.attype)
                    if system.diameter(j)==0
                        system.diameter(j)=1;
                    end
                end

                Write_XML_Nano(0,savename,system)
            end
        end
    end
else
    system=Extract_XML([path File]);
    for j=1:length(system.attype)
        if system.diameter(j)==0
            system.diameter(j)=1;
        end
    end
    savename=[path File(1:end-4) '_vis.xml'];
    Write_XML_Nano(0,savename,system)
end