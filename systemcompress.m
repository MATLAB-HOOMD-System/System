function system=systemcompress(attype,bonds,dim,mass,pos,angles,chainstats,body)
system.attype=attype;
system.bonds=bonds;
system.dim=dim;
system.mass=mass;
system.pos=pos;
system.angles=angles;
if exist('chainstats','var')
    system.chainstats=chainstats;
end
if exist('body','var')
    system.body=body;
end