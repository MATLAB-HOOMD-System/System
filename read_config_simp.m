clear pos attype bonds
dim=[];
numAbeads=4;
numBbeads=4;
beadsperchain=numAbeads+numBbeads;
clc
file1='/home/andrew/simpatico/examples/diblock/N32/mc/out/config2';
fid1=fopen(file1,'r');
% for i=1:11
%     x=fgetl(fid1);
%     x
% end



L=1;
flag='box';

while strcmp(flag,'box')
            x=fgetl(fid1)
            if length(x)>=5
                if x(1:5)=='cubic';
                    ind=6;
    %                 flag2=0;
                    while x(ind)==' '
                        ind=ind+1;
                    end
                    boxsize=str2num(x(ind:end));
                    flag='molecules'
                elseif length(x)>=12
                    if x(1:12)=='orthorhombic';
                        dim=str2num(x(13:end));
                        flag='molecules'
                    end
                end
                    
            end
                
end
beadnum=1;
attype=[];
bonds=[];
while strcmp(flag,'molecules')
            x=fgetl(fid1);
            if length(x)>=7
                if ~isempty(str2num(x(7)));
                    X=x(3:end);

                   spaces=find(X==' ');
                   pos(beadnum,:)=str2num(X);%[str2num(X(1:spaces(1)-1)),str2num(X(spaces(1):spaces(3)-1)),str2num(X(spaces(3):end))];
                   if mod(beadnum,beadsperchain)<=numAbeads
                        attype=[attype; 'A'];
                        if mod(beadnum,beadsperchain)~=1
                            bonds=[bonds; beadnum-1,beadnum];
                        end
                   else
                       attype=[attype; 'B'];
                       bonds=[bonds; beadnum-2,beadnum-1];
                   end
                   beadnum=beadnum+1;
                end
            end
            if x==-1 | strcmp(x,'TETHERS')
                flag='end'
            end
end

angles=[];
mass=ones(length(attype),1);
if isempty(dim)
    dim=[boxsize,boxsize,boxsize];
end
systemcompress_script
Write_XML(0,'testMC.xml',system)