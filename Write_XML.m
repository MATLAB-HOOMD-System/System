function Write_XML(File_Name,Output_Name,system)
%Write_XML(File_Name,Output_Name,system)
% Writes an XML file from a .mat file, File_Name. It will be saved as
% "Output_Name". Make sure "Output_Name" has a .xml if you want an
% extension.
%system should 
if File_Name~=0
    load(File_Name,'-mat');
else
    [attype,bonds,dim,mass,pos,angles, chainstats,body,vel,orientation,moment_inertia,angmom]=systemexpand(system);
end

nat=size(pos,1);

fid=fopen(Output_Name,'w','l');

fprintf(fid,'<?xml version="1.0" encoding="UTF-8"?>\n');
fprintf(fid,'<hoomd_xml version="1.7">\n');
fprintf(fid,'<configuration time_step="0" dimensions="3" natoms="%1.0f" >\n',nat);
fprintf(fid,'<box lx="%1.4f" ly="%1.4f" lz="%1.4f" xy="0" xz="0" yz="0"/>\n',dim);

fprintf(fid,'<position num="%1.0f">\n',nat);
fprintf(fid,'%8.4f %8.4f %8.4f\n',pos');
fprintf(fid,'</position>\n');

disp('vel')

if exist('vel','var')
    fprintf(fid,'<velocity num="%1.0f">\n',nat);
    fprintf(fid,'%8.4f %8.4f %8.4f\n',vel');
    fprintf(fid,'</velocity>\n');
end

disp('acc')

if exist('acc','var')
    fprintf(fid,'<acceleration num="%1.0f">\n',nat);
    fprintf(fid,'%8.4f %8.4f %8.4f\n',acc');
    fprintf(fid,'</acceleration>\n');
end

% if exist('attype','var') && ~exist('mass','var')
%     A=attype(:,:)=='A';
%     x=attype(:,:)=='C';
%     A=A+x;
%     B=attype(:,:)=='B';
%     x=attype(:,:)=='D';
%     B=B+x;
%     C=~A & ~ B;
% %     mass=95.717*A+99.57*B+(99.57+95.717)/2*C;
% end
disp('mass')

if exist('mass','var')
    fprintf(fid,'<mass num="%1.0f">\n',nat);
    fprintf(fid,'%6.3f\n',mass);
    fprintf(fid,'</mass>\n');
end

disp('attype')
if exist('attype','var')
    fprintf(fid,'<type num="%1.0f">\n',nat);
    for i=1:nat
       fprintf(fid,'%1s\n',attype(i,1));
    end
    fprintf(fid,'</type>\n');
end

disp('body')
if exist('body')%~=[]
    fprintf(fid,'<body num="%1.0f">\n',nat);
    fprintf(fid,'%i\n',body');
    fprintf(fid,'</body>\n');
end

disp('orient')

if exist('orientation','var')
    fprintf(fid,'<orientation num="%1.0f">\n',nat);
    fprintf(fid,'%8.4f %8.4f %8.4f %8.4f\n',orientation');
    fprintf(fid,'</orientation>\n');
end

disp('angmom')
if exist('angmom','var')
    fprintf(fid,'<angmom num="%1.0f">\n',nat);
    fprintf(fid,'%8.4f %8.4f %8.4f %8.4f\n',angmom');
    fprintf(fid,'</angmom>\n');
end

disp('momintert')
if exist('moment_inertia','var')
    fprintf(fid,'<moment_inertia num="%1.0f">\n',nat);
    fprintf(fid,'%8.4f %8.4f %8.4f\n',moment_inertia');
    fprintf(fid,'</moment_inertia>\n');
end

disp('bonds')
if exist('bonds','var')
    [nbond col]=size(bonds);
    fprintf(fid,'<bond num="%1.0f">\n',nbond);
    for i=1:nbond
%         fprintf(fid,['polymer' attype(bonds(i,1)+1,1) ' %1.0f %1.0f\n'],bonds(i,:));
        fprintf(fid,['polymer' ' %1.0f %1.0f\n'],bonds(i,:));
    end
    fprintf(fid,'</bond>\n');
end

disp('angles')
if exist('angles','var')
    [nangle col]=size(angles);
    fprintf(fid,'<angle num="%1.0f">\n',nangle);
    for i=1:nangle
        x=attype(angles(i,2)+1,1);
        fprintf(fid,[x '-' x '-' x ' %1.0f %1.0f %1.0f\n'],angles(i,:));
    end
    fprintf(fid,'</angle>\n');
end

% if exist('vel','var')
%     fprintf(fid,'<velocity num="%1.0f">\n',nat);
%     fprintf(fid,'%8.4f %8.4f %8.4f\n',vel');
%     fprintf(fid,'</velocity>\n');
% end



fprintf(fid,'</configuration>\n');
fprintf(fid,'</hoomd_xml>\n');
fclose(fid);
% toc
end