path='/media/andrew/External_Data/Google Drive LATECH/simulations/Transfer_to_Shared/Nanoparticles/PCND_repulsive_3_8_18/'
path='/mnt/Shared_Data/Nanoparticles/PCND_repulsive/'
Files=dir(path);

for i=1:length(Files)
    if length(Files(i).name)>10
        if Files(i).name(end-9:end)=='_start.xml'
            xmlname=[path Files(i).name];
            savename=[path Files(i).name(1:end-10) '_final.xml'];
            dcdname=[path Files(i).name(1:end-10) '_traj1.dcd'];
            nsteps=readdcd(dcdname,0,1);
            system=Extract_XML_DCD(xmlname,dcdname,[],nsteps)
%             system=Extract_XML([path Files(i).name])
            
            Write_XML_Nano(0,savename,system)
        end
    end
end


