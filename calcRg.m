function [ Rg ] = calcRg( pos,type )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
    [numbeads a]=size(pos);
    if exist('type','var')==0
        rmean=mean(pos);
        r=0;
        for i=1:numbeads
            r=r+sum((pos(i,:)-rmean).^2);
        end
        Rg=sqrt(r/numbeads);
   elseif strcmp(type,'Rg')
        rmean=mean(pos);
        r=0;
        for i=1:numbeads
            r=r+sum((pos(i,:)-rmean).^2);
        end
        Rg=sqrt(r/numbeads);
    elseif strcmp(type,'Rend')
        Rg=sqrt(sum((pos(1,:)-pos(end,:)).^2));
    elseif strcmp(type,'Reigxz')
        [nbeads ndim]=size(pos);
        for i=1:nbeads-1
            bonds(i,:)=[i-1 i];
        end
        for i=1:nbeads
            attype(i)='B';
            mass(i)=1;
        end
        system.pos=pos;
        system.bonds=bonds;
        system.dim=[1e10 1e10 1e10];
        system.attype=attype;
        system.mass=mass;
        system.angles=zeros(0,3);
        
        chainstats=Chain_Statistics_single(0,system);
        Rg=chainstats.Rg_eigenvalues;
        Rg=abs(max(Rg)/min(Rg));
    elseif strcmp(type,'Reigxy')
%         pause(0.1);
        [nbeads ndim]=size(pos);
        for i=1:nbeads-1
            bonds(i,:)=[i-1 i];
        end
        for i=1:nbeads
            attype(i)='B';
            mass(i)=1;
        end
        system.pos=pos;
        system.bonds=bonds;
        system.dim=[1e10 1e10 1e10];
        system.attype=attype;
        system.mass=mass;
        system.angles=zeros(0,3);
        
        chainstats=Chain_Statistics_single(0,system);
        Rg=chainstats.Rg_eigenvalues;
        Rg=abs(Rg(3)/Rg(2));
        
    else
        error('invalid type')
    end
        
end

