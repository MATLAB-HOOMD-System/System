function [com,positions]=COM(Chain,pos,dim)
%center of mass calculation
if exist('dim','var')==0
    dim=[30 30 30];
end

    positions=pos(Chain,:);
    
    distances=positions-positions(1,:);
    for i=1:3
       positions(distances(:,i)>dim(i)/2,i)=positions(distances(:,i)>dim(i)/2,i)-dim(i);
       positions(distances(:,i)<-dim(i)/2,i)=positions(distances(:,i)<-dim(i)/2,i)+dim(i);
    end
%     distances=sort(sum(distances.^2,2)).^0.5;
    
    
    com=mean(positions);

end