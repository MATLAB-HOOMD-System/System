function [mindis] = minimumdistanceimage(pos,dim)
%minimumdistanceimage finds the minimum distance using the minimum image
%convention. Box is centered at 0 0 0. 
n=size(pos,1);
mindis=inf;
for i=1:n
    for j=1:n
        i;
        j;
        if i==j
            dis=inf;
        else
            disxyz=pos(i,:)-pos(j,:);
            for p=1:3
                if abs(disxyz(p))>dim(p)/2;
                    disxyz(p)=dim(p)-disxyz(p);
                end
            end
            dis=sqrt(sum((disxyz).^2));
        end
        if dis<mindis
            mindis=dis;
        end
    end
end

